package com.fariasmaxi.actions

import com.fariasmaxi.core.domain.Product
import com.fariasmaxi.core.domain.ProductRepository
import com.fariasmaxi.infrastructure.InMemoryPersistedProduct
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.mock
import org.amshove.kluent.Verify
import org.amshove.kluent.When
import org.amshove.kluent.called
import org.amshove.kluent.calling
import org.amshove.kluent.itReturns
import org.amshove.kluent.on
import org.amshove.kluent.that
import org.amshove.kluent.was
import org.junit.Before
import org.junit.Test


class AddProductShould {

	private lateinit var addProduct: AddProduct
	private lateinit var productRepository: ProductRepository

	@Before
	fun setUp() {
		productRepository = mock()
		addProduct = AddProduct(productRepository)
	}

	@Test
	fun `add and return him`() {
		val productName = givenAProduct("Some Product")
		val newProduct = whenAddProduct(productName)
		thenItIsAdded(newProduct.name)
	}

	private fun givenAProduct(productName: String): String {
		When calling productRepository.save(eq(productName)) itReturns InMemoryPersistedProduct(1L, productName)
		return productName
	}

	private fun whenAddProduct(productName: String): Product {
		return addProduct.doAction(productName)
	}

	private fun thenItIsAdded(productName: String) {
		Verify on productRepository that productRepository.save(productName) was called
	}
}