package com.fariasmaxi.actions

import com.fariasmaxi.core.domain.Product
import com.fariasmaxi.core.domain.ProductRepository
import com.fariasmaxi.infrastructure.InMemoryPersistedProduct
import com.nhaarman.mockito_kotlin.mock
import org.amshove.kluent.When
import org.amshove.kluent.`should not be`
import org.amshove.kluent.calling
import org.amshove.kluent.itReturns
import org.junit.Before
import org.junit.Test
import kotlin.test.fail

class GetProductShould {


	private lateinit var getProduct: GetProduct
	private lateinit var productRepository: ProductRepository

	@Before
	fun setUp() {
		productRepository = mock()
		getProduct = GetProduct(productRepository)
	}

	@Test
	fun `return him if exists`() {
		givenAnExistingProduct(1L)
		val product = whenGetProduct(1L)
		thenItIsRetrieved(product)
	}

	@Test(expected = NonExistingProductException::class)
	fun `fails with a non existing product`() {
		whenGetProduct(20L)
		fail("should be raise non existing product exception!")
	}

	private fun givenAnExistingProduct(id: Long) {
		When calling productRepository.get(id) itReturns InMemoryPersistedProduct(id, "Some Product")
	}

	private fun whenGetProduct(id: Long): Product {
		return getProduct.doAction(id)
	}

	private fun thenItIsRetrieved(product: Product?) {
		product `should not be` null
	}
}