package com.fariasmaxi.infrastructure.rest

import com.fariasmaxi.Application
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [Application::class, ProductResource::class],
		webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
open class ProductResourceShould : SpringBaseTest() {
	private val ProductResponseType = ProductResponse::class.java

	@Autowired
	lateinit var testRestTemplate: TestRestTemplate

	@Test
	fun `add new product`() {
		val productRequest = givenANewProductRequest()
		val response = whenAddProduct(productRequest)
		thenItIsAdded(response, productRequest)
	}

	@Test
	fun `get existing product`() {
		val productRequest = givenANewProductRequest()
		whenAddProduct(productRequest)

		val response = whenGetAProduct()
		thenItIsAdded(response, productRequest)
	}

	private fun givenANewProductRequest(): ProductRequest {
		return ProductRequest("A book")
	}

	private fun whenAddProduct(productRequest: ProductRequest): ResponseEntity<ProductResponse> {
		return testRestTemplate.postForEntity<ProductResponse>("/products", productRequest, ProductResponseType)
	}

	private fun whenGetAProduct(): ResponseEntity<ProductResponse> {
		return testRestTemplate.getForEntity<ProductResponse>("/products/1", ProductResponseType)
	}

	private fun thenItIsAdded(response: ResponseEntity<ProductResponse>, productRequest: ProductRequest) {
		assertTrue(response.body?.name == productRequest.name)
	}
}
