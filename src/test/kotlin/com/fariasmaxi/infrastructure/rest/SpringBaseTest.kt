package com.fariasmaxi.infrastructure.rest

import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import redis.clients.jedis.Jedis

@RunWith(SpringJUnit4ClassRunner::class)
@ActiveProfiles("test")
@Import(SpringTestConfiguration::class)
abstract class SpringBaseTest {
	companion object {
		private var redisBaseTest: RedisTestService = RedisTestService()

		@BeforeClass
		@JvmStatic
		fun beforeClass() {
			redisBaseTest.startRedis()
		}

		@AfterClass
		@JvmStatic
		fun afterClass() {
			redisBaseTest.stopRedis()
		}
	}

	@Autowired
	lateinit var jedis: Jedis

	@Before
	fun before() {
		jedis.flushAll()
	}
}

@Configuration
open class SpringTestConfiguration {

	@Bean
	open fun redisBaseTest(): RedisTestService {
		return RedisTestService()
	}

}
