package com.fariasmaxi.infrastructure.rest

import org.assertj.core.util.Lists
import redis.embedded.RedisServer
import java.io.IOException

class RedisTestService {

	private val redisServers = Lists.newArrayList<RedisServer>()

	fun startRedis() {
		startServer()
	}

	private fun startServer() {
		try {
			val redisServer = RedisServer.builder().build()
			redisServer.start()
			redisServers.add(redisServer)
		} catch (e: IOException) {
			e.printStackTrace()
			println("Could not create test redis")
		}

	}

	fun stopRedis() {
		try {
			for (redisServer in redisServers) {
				redisServer.stop()
			}
		} catch (e: InterruptedException) {
			println("Error stopping redisServer")
		}
		Runtime.getRuntime().addShutdownHook(Thread { redisServers.forEach { server -> server.stop() } })
	}
}
