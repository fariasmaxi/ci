package com.fariasmaxi.actions

import com.fariasmaxi.core.domain.PersistedProduct
import com.fariasmaxi.core.domain.Product
import com.fariasmaxi.core.domain.ProductRepository

class GetProduct(private val productRepository: ProductRepository) {

	fun doAction(id: Long): Product {
		return getPersistedProduct(id)?.let { mapToProduct(it) }
				?: throw NonExistingProductException()
	}

	private fun getPersistedProduct(id: Long) = productRepository.get(id)
	private fun mapToProduct(it: PersistedProduct) = Product(it.id, it.name)
}

class NonExistingProductException : Throwable()
