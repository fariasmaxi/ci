package com.fariasmaxi.actions

import com.fariasmaxi.core.domain.Product
import com.fariasmaxi.core.domain.ProductRepository

class AddProduct(private val productRepository: ProductRepository) {
	fun doAction(name: String): Product {
		return productRepository.save(name).let { Product(it.id, it.name) }
	}
}