package com.fariasmaxi.configuration

import com.fariasmaxi.actions.AddProduct
import com.fariasmaxi.actions.GetProduct
import com.fariasmaxi.core.domain.ProductRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
open class ProductConfiguration {
	@Bean
	open fun addProduct(productRepository: ProductRepository): AddProduct {
		return AddProduct(productRepository)
	}

	@Bean
	open fun getProduct(productRepository: ProductRepository): GetProduct {
		return GetProduct(productRepository)
	}
}
