package com.fariasmaxi.infrastructure

import com.fariasmaxi.core.domain.ProductRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import redis.clients.jedis.Jedis

@Configuration
open class InfrastructureConfiguration {

	@Bean
	open fun jedis(): Jedis {
		return Jedis("localhost")
	}

	@Bean
	open fun redisBookRepository(jedis: Jedis): ProductRepository {
		return RedisProductRepository(jedis)
	}
}