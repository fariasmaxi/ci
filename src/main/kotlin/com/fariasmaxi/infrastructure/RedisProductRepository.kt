package com.fariasmaxi.infrastructure

import com.fariasmaxi.core.domain.PersistedProduct
import com.fariasmaxi.core.domain.ProductRepository
import redis.clients.jedis.Jedis

class RedisProductRepository(private val redis: Jedis) : ProductRepository {

	private val key = "products"

	override fun save(productName: String): PersistedProduct {
		val id = redis.incr("product_ids")
		redis.hset(key, id.toString(), productName)
		return RedisPersistedProduct(id, productName)
	}

	override fun get(id: Long): PersistedProduct? {
		return redis.hget(key, id.toString())?.let { RedisPersistedProduct(id, it) }
	}
}

data class RedisPersistedProduct(override val id: Long,
								 override val name: String) : PersistedProduct