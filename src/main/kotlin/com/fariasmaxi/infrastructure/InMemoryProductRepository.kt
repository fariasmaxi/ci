package com.fariasmaxi.infrastructure

import com.fariasmaxi.core.domain.PersistedProduct
import com.fariasmaxi.core.domain.ProductRepository

data class InMemoryPersistedProduct(override val id: Long, override val name: String) : PersistedProduct