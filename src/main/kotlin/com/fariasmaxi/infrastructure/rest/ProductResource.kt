package com.fariasmaxi.infrastructure.rest

import com.fariasmaxi.actions.AddProduct
import com.fariasmaxi.actions.GetProduct
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/products")
open class ProductResource(private val addProduct: AddProduct,
						   private val getProduct: GetProduct) {
	@PostMapping
	fun addNew(@RequestBody product: ProductRequest): ProductResponse {
		return addProduct.doAction(product.name).let {
			ProductResponse(it.id, it.name)
		}
	}

	@GetMapping("/{id}")
	fun getProductBy(@PathVariable id: Long): ProductResponse {
		return getProduct.doAction(id).let { ProductResponse(it.id, it.name) }
	}
}

data class ProductResponse(val id: Long, val name: String)
data class ProductRequest(val name: String)