package com.fariasmaxi.core.domain

data class Product(val id: Long, val name: String)