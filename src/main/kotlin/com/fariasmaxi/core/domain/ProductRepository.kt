package com.fariasmaxi.core.domain

interface ProductRepository {
	fun save(productName: String): PersistedProduct
	fun get(id: Long): PersistedProduct?
}