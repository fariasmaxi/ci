package com.fariasmaxi.core.domain

interface PersistedProduct {
	val id: Long
	val name: String
}